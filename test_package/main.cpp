#include <tensorflow/lite/interpreter.h>
#include <tensorflow/lite/kernels/register.h>
#include <tensorflow/lite/model.h>
#include <tensorflow/lite/tools/gen_op_registration.h>

#include <cmath>
#include <cstdio>

#define ASSERT_ALMOST_EQ(v1, v2)                                               \
  do {                                                                         \
    if (fabs(v1 - v2) > 1e-4) {                                                \
      printf("Values do not match: %f vs %f\n", v1, v2);                       \
      return -1;                                                               \
    }                                                                          \
  } while (0)

#define ASSERT_EQ(v1, v2)                                                      \
  do {                                                                         \
    if (v1 != v2) {                                                            \
      printf("Values do not match\n");                                         \
      return -1;                                                               \
    }                                                                          \
  } while (0)

template <typename T> T relu(T x) { return x <= T(0) ? T(0) : x; }

int main() {
  std::unique_ptr<tflite::FlatBufferModel> model =
      tflite::FlatBufferModel::BuildFromFile("relu.tflite");

  if (!model) {
    printf("Failed to mmap model\n");
    exit(0);
  }

  tflite::ops::builtin::BuiltinOpResolver resolver;
  std::unique_ptr<tflite::Interpreter> interpreter;
  tflite::InterpreterBuilder(*model.get(), resolver)(&interpreter);

  // Resize input tensors, if desired.
  interpreter->AllocateTensors();

  const auto inputTensor = interpreter->tensor(interpreter->inputs()[0]);
  size_t numEl = 1;
  for (size_t i = 0; i < inputTensor->dims->size; ++i) {
    numEl *= inputTensor->dims->data[i];
  }

  float *input = interpreter->typed_input_tensor<float>(0);
  // Dummy input for testing
  for (size_t i = 0; i < numEl; ++i) {
    input[i] = static_cast<float>(rand()) / static_cast<float>(RAND_MAX) - 0.5f;
  }

  interpreter->Invoke();

  float *output = interpreter->typed_output_tensor<float>(0);

  for (size_t i = 0; i < numEl; ++i) {
    ASSERT_ALMOST_EQ(output[0], (0.5 + relu(input[0])) * 1.3);
  }

  // check that input and output dimensions match
  const auto outputTensor = interpreter->tensor(interpreter->outputs()[0]);
  ASSERT_EQ(outputTensor->dims->size, inputTensor->dims->size);
  for (size_t i = 0; i < outputTensor->dims->size; ++i) {
    ASSERT_EQ(outputTensor->dims->data[i], inputTensor->dims->data[i]);
  }

  printf("--- TEST PASSED! ---\n");

  return 0;
}
