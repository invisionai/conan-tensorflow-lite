# ==== Creates a simple tflite model that does (0.5 + relu(x)) * 1.3 ====

import tensorflow as tf
import tensorflow.lite as lite
import numpy as np

# == Prepare tensorflow ==
data_format = 'channels_last'
tf.keras.backend.set_image_data_format(data_format)

from tensorflow.python.keras.api._v2.keras import layers, models
from tensorflow.python.keras.engine.base_layer import Layer

@tf.function
def forward(input):
    return (0.5 + tf.nn.relu(input)) * 1.3

input = tf.convert_to_tensor(np.zeros((1, 224, 224, 3), dtype=np.float32))

concrete_fn = forward.get_concrete_function(input)
converter = lite.TFLiteConverter.from_concrete_functions([concrete_fn])
tflite_model = converter.convert()

with open("relu.tflite", "wb") as f:
    f.write(tflite_model)
