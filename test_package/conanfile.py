import os
import shutil

from conans import ConanFile, CMake, tools


class TFLiteTestConan(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def test(self):
        # copy tflite file
        shutil.copyfile(src=self.source_folder + '/relu.tflite', dst=self.build_folder + '/bin/relu.tflite')        

        os.chdir("bin")
        self.run("./test")
