import sys
from os import getcwd
from multiprocessing import cpu_count
from conans import ConanFile, tools, AutoToolsBuildEnvironment

import shutil
import os
import glob


class TFLiteConan(ConanFile):
    name = "tensorflow-lite"
    lib_version = "2.1"
    recipe_revision = "0"
    version = lib_version + '-' + recipe_revision
    repo_url = "https://github.com/tensorflow/tensorflow.git"

    description = "https://www.tensorflow.org/"
    license = "Apache-2.0"

    # Conan build process settings
    exports = [ "LICENSE.md" ] 
    settings = "os", "arch", "compiler", "build_type"
    options = { "shared": [True, False], "fPIC": [True, False] }
    default_options = { "shared": True, "fPIC": True }
    source_subfolder = "tensorflow"
    build_subfolder = "tensorflow/lite/tools/make" # relative path
    generators = "make"

    def requirements(self):
        self.requires('flatbuffers/[>=1.11.0]@google/stable')
        self.requires('abseil/[>=20181200]@invisionai/stable')

    def config_options(self):
        if self.settings.os == 'Windows':
            del self.options.fPIC

    def system_requirements(self):
        if self.settings.os == "Linux":
            package_names = ["curl", "unzip"]  # needed to download dependencies for tensorflow-lite's scripts
            installer = tools.SystemPackageTool()
            for package_name in package_names:
                installer.install(package_name)

    def source(self):
        git = tools.Git(folder=self.source_subfolder)
        git.clone(self.repo_url, shallow=True, branch=f"r{self.lib_version}")

    def build(self):
        with tools.chdir(self.source_subfolder):
            env_build = AutoToolsBuildEnvironment(self)
            env_build.fpic = True
            #env_build.libs.append("pthread")
            env_build.defines.append("BUILD_WITH_NNAPI=false")
            env_build.defines.append("BUILD_WITH_MMAP=false")

            env_build_vars = env_build.vars
            env_build_vars['BUILD_WITH_NNAPI'] = 'false'
            env_build_vars['BUILD_WITH_MMAP'] = 'false'

            # conan stores include paths in CPPFLAGS, but tflite is using CXXFLAGS
            # let's merge them
            tools.replace_in_file(f"{self.build_subfolder}/Makefile", 
                "CXXFLAGS := -O3 -DNDEBUG -fPIC", "CXXFLAGS += ${CPPFLAGS}")

            # similar for LIBS
            tools.replace_in_file(f"{self.build_subfolder}/Makefile",
                'LIBS := \\', 'LIBS += \\')

            # flatbuffers used from conan, not internally
            tools.replace_in_file(f"{self.build_subfolder}/Makefile", 
                'tensorflow/lite/tools/make/downloads/flatbuffers/src/util.cpp', '')

            # remove a file that contains a main() method from compilation.
            # (TFLite's Makefile is quite messy and probably they forgot to take care of that)
            tools.replace_in_file(f"{self.build_subfolder}/Makefile", 
                "$(wildcard tensorflow/lite/*test.cc) \\",
                "$(wildcard tensorflow/lite/*test.cc) \\\ntensorflow/lite/experimental/ruy/tune_tool.cc \\")

            # IMPORTANT: we let tflite link against its own header-only libs and libs that
            # are unlikely to be used by other libraries, so that it's easier to maintain this recipe
            # -- No abseil, use conan one
            tools.replace_in_file(f"{self.build_subfolder}/download_dependencies.sh", 
                'download_and_extract "${ABSL_URL}" "${DOWNLOADS_DIR}/absl"',
                '')
            # -- No google test as we don't run the tests
            tools.replace_in_file(f"{self.build_subfolder}/download_dependencies.sh", 
                'download_and_extract "${GOOGLETEST_URL}" "${DOWNLOADS_DIR}/googletest"',
                '')
            # -- No flatbuffers, use conan one
            tools.replace_in_file(f"{self.build_subfolder}/download_dependencies.sh", 
                'download_and_extract "${FLATBUFFERS_URL}" "${DOWNLOADS_DIR}/flatbuffers"',
                '')

            # -- Eigen: we use the internal version that tf checks out, as they patch it for some reason

            with tools.chdir(self.build_subfolder):
                self.run("./download_dependencies.sh")

            build_target = "all" # "micro"?
            env_build.make(target="%s -f %s/Makefile -C %s" % (
                build_target, 
                self.build_subfolder,
                getcwd()),
                vars=env_build_vars)

    def package(self):
        self.copy(pattern="LICENSE", dst="licenses", src=self.source_subfolder)
        self.copy(pattern="*.a*", dst="lib", src='tensorflow/tensorflow/lite/tools/make/gen', keep_path=False, symlinks=True)

        os.makedirs(self.package_folder + '/include/tensorflow/lite')
        src_files = glob.glob(self.build_folder + '/tensorflow/tensorflow/lite/*.h')
        for src_file in src_files:
            dst_file = self.package_folder + '/include/tensorflow/lite/' + os.path.basename(src_file)
            shutil.copyfile(src_file, dst_file)

        pt = ['kernels/*.h', 'c/*.h', 'api/*.h', 'core/*.h', 
            'delegates/*.h', 'schema/*.h', 'tools/*.h', 'experimental/*.h']
        for pattern in pt:
            self.copy(pattern=pattern, dst="include/tensorflow/lite", src='tensorflow/tensorflow/lite/', keep_path=True, symlinks=True)

    def package_info(self):
        self.cpp_info.libs = ["tensorflow-lite"]
